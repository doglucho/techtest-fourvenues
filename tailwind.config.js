export const darkMode = 'class';

export const content = [
  './src/**/*.html',
  './src/**/*.ts',
  './index.html',
];

export const theme = {
  extend: {
    colors: {
      dark: {
        primary: '#000000',
        secondary: '#111111',
      },
    },
  },
};