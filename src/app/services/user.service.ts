import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})

export class UserService {
  private userApiUrl = 'https://randomuser.me/api/';

  constructor(private httpClient: HttpClient) { }

  getRandomUser(): Observable<any> {
    return this.httpClient.get(this.userApiUrl);
  }
}
