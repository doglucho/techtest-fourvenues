import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root',
})

export class ThemeService {
  isDarkTheme: boolean = false;
  private themeChanged = new Subject<boolean>();
  themeChanged$ = this.themeChanged.asObservable();

  constructor() { }

  toggleTheme(): void {
    this.isDarkTheme = !this.isDarkTheme;
    this.themeChanged.next(this.isDarkTheme);
    console.log('Theme Changed:', this.isDarkTheme);
  }
}
