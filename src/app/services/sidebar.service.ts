import { EventEmitter, Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})

export class SidebarService {
  sidebarHidden: boolean = false;
  sidebarVisibilityChanged: EventEmitter<boolean> = new EventEmitter<boolean>();

  constructor() { }

  toggleSidebarVisibility(): void {
    this.sidebarHidden = !this.sidebarHidden;
    this.sidebarVisibilityChanged.emit(this.sidebarHidden);
  }
}
