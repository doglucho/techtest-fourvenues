import { Component, Injectable } from '@angular/core';
import { SidebarService } from 'src/app/services/sidebar.service';
import { ThemeService } from 'src/app/services/theme.service';

@Injectable()

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss']
})

export class SidebarComponent {
  sidebarVisible: boolean = true;
  selectedContent: string = '';

  constructor(private sidebarService: SidebarService, public themeService: ThemeService) { }

  ngOnInit() {
  }
  
  hideSidebar(): any {
    this.sidebarService.toggleSidebarVisibility();
  }

  showContent(content: string): void {
    this.selectedContent = content;
  }
}
