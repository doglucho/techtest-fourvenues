import { Component, EventEmitter, Injectable, HostListener, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { SidebarService } from 'src/app/services/sidebar.service';
import { ThemeService } from 'src/app/services/theme.service';

@Injectable()

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})

export class DashboardComponent implements OnInit, OnDestroy {
  isMenuOpen: boolean = false;
  isUserMenuOpen: boolean = false;
  toggleSidebarEvent: any = new EventEmitter();
  isMobile: boolean = false;
  mobileBreakpoint = 768;
  dropdownVisible: boolean = false;
  dropdownVisibleContacts: boolean = false;
  dropdownVisibleChartMenu: boolean = false;
  dropdownVisibleFirstTableMenu: boolean = false;
  dropdownVisibleSecondTableMenu: boolean = false;
  isSidebarHidden: boolean = false;
  private sidebarSubscription?: Subscription;
  
  constructor(private sidebarService: SidebarService, public themeService: ThemeService) {
    this.updateIsMobile();
  }

  ngOnInit() {
    this.sidebarSubscription = this.sidebarService.sidebarVisibilityChanged.subscribe(
      (hidden: boolean) => {
        this.isSidebarHidden = hidden;
      }
    );
  }

  ngOnDestroy() {
    if (this.sidebarSubscription) {
      this.sidebarSubscription.unsubscribe();
    }
  }

  toggleMenu(): void {
    this.isMenuOpen = !this.isMenuOpen;
  }

  toggleDropdown(): void {
    this.dropdownVisible = !this.dropdownVisible;
  }

  toggleDropdownContacts(): void {
    this.dropdownVisibleContacts = !this.dropdownVisibleContacts;
  }

  toggleDropdownChartMenu(): void {
    this.dropdownVisibleChartMenu = !this.dropdownVisibleChartMenu;
  }

  toggleDropdownFirstTableMenu(): void {
    this.dropdownVisibleFirstTableMenu = !this.dropdownVisibleFirstTableMenu;
  }

  toggleDropdownSecondTableMenu(): void {
    this.dropdownVisibleSecondTableMenu = !this.dropdownVisibleSecondTableMenu;
  }

  toggleUserMenu(): void {
    this.isUserMenuOpen = !this.isUserMenuOpen;
  }
  
  showSidebar() {
    this.sidebarService.toggleSidebarVisibility();
  }

  @HostListener('window:resize')
  onWindowResize() {
    this.updateIsMobile();
  }

  private updateIsMobile() {
    this.isMobile = window.innerWidth < this.mobileBreakpoint;
  }
}
