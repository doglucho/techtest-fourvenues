import { Component, OnInit } from '@angular/core';
import { Subscription, interval, switchMap } from 'rxjs';
import { ThemeService } from 'src/app/services/theme.service';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})

export class NavbarComponent implements OnInit {
  isMenuOpen: boolean = false;
  isUserMenuOpen: boolean = false;
  randomUser?: any = {};
  subscription!: Subscription;
  isDarkTheme: boolean = false;

  constructor(private userService: UserService, public themeService: ThemeService) { }

  ngOnInit() {
    this.themeService.themeChanged$.subscribe((isDarkTheme) => {
      this.isDarkTheme = isDarkTheme;
    });

    this.getRandomUser();

    this.subscription = interval(10000)
    .pipe(
      switchMap(() => this.userService.getRandomUser())
    )
    .subscribe({
      next: (data: any) => {
        this.randomUser = data.results[0];
        console.log("Get Users Interval", this.randomUser);
      },
      error: (error) => {
        console.log(error);
      }
    });
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  getRandomUser(): void {
    this.userService.getRandomUser().subscribe({
      next: (data: any) => {
        this.randomUser = data.results[0];
        console.log("Get Users", this.randomUser);
      },
      error: (error) => {
        console.log(error);
      }
    });
  }

  toggleMenu(): void {
    this.isMenuOpen = !this.isMenuOpen;
  }

  toggleUserMenu(): void {
    this.isUserMenuOpen = !this.isUserMenuOpen;
  }
}
